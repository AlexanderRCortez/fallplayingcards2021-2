
// Playing Cards
// Devon Lozier
// Alex

#include <iostream>
#include <conio.h>

using namespace std;

enum class Ranks
{
	two = 2,
	three,
	four,
	five,
	six,
	seven,
	eight,
	nine,
	ten,
	jack,
	queen,
	king,
	ace,

};
enum class Suits
{
	clubs,
	diamonds,
	hearts,
	spades
};


struct Card
{
	Ranks Rank;
	Suits Suit;
};

int main()
{
	Card c1;
	c1.Rank = Ranks::ace;
	c1.Suit = Suits::spades;

	(void)_getch();
	return 0;
}
